# baltini


## Notes
- When I saw a couple of line on sql (question) file, it written using mysql flavor on mysql db, so I decided to using mysql environment on my local
- If you want to make daily base, you can edit the
```
df = pd.read_sql('SELECT * FROM products order by updated_at desc', con=db_connection)
# INTO
df = pd.read_sql('SELECT * FROM products where updated_at = datetime.now()', con=db_connection)
# change datetime now to YYYY-MM-DD %HH:%MM:%SS format first
```
- Quoted on question "insert a data to product_duplicates and product_duplicate_details table" there's no product_duplicate_details table but product_duplicate_lists
## Anomaly

Actually, there's a couple of anomaly that still haunt me, here's the reasons :
- You can check with this query
```
select * from products p where title = 'MONCLER logo-patch cotton T-shirt';
```
you put constraint unique on product_duplicates table specifically on title column, is it true title? I think it's not possible
why? because even it have same title but product_id, gender, and category was different, so I thought it was basically different product
but with under same umbrella (group title)

