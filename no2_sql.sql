
SELECT DISTINCT  title AS Group_Title, count(*) AS Products_Count, updated_at 
FROM products p 
GROUP BY title, updated_at 
ORDER BY products_count DESC;