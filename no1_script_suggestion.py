from sqlalchemy import create_engine
import pandas as pd
import re
from pandas.io import sql

def get_productid(row) :
    result = dict(re.findall(r'(\S+):(\S+)', row['tags'].replace(": ", ":").replace(",", " ")))
    if 'ProductID' in result.keys():
        result['ProductID'] = result['ProductID'][:-1]
        return result['ProductID']
    else :
        return 'no_id'

db_connection_str = 'mysql+pymysql://<your_username>:<your_pass>@localhost/baltini'
db_connection = create_engine(db_connection_str)

# order by updated at to make sure we took the updated data before we drop it
#----------
df = pd.read_sql('SELECT * FROM products order by updated_at desc', con=db_connection)
df['product_id'] = df.apply(get_productid, axis=1)
#----------
# if you want to check duplicated data
# df['duplicates'] = df.duplicated(subset=['product_id', 'gender', 'category'], keep='first')
# df2 = df.loc[df['duplicates'] == True]
# print(df.loc[df['duplicates'] == True])
# print(len(df2.index))
#---------
df = df.drop_duplicates(subset=['product_id', 'gender', 'category'], keep='first')
#----------
df1 = df[['id', 'title', 'created_at', 'updated_at']]
df1.to_sql('product_duplicates', con=db_connection, if_exists='append', index=False)
df['product_duplicate_id'] = df['id']
df2 = df[['id', 'product_duplicate_id', 'external_id', 'product_id', 'deleted_at', 'created_at', 'updated_at']]
df2.to_sql('product_duplicate_lists', con=db_connection, if_exists='append', index=False)